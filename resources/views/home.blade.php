<!DOCTYPE html>
<html>
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Correo electrónico</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>
<body class="container">
    @if(session('success'))
        <div class="alert alert-success" role="alert">
          <h4 class="alert-heading"></h4>
          <p>{{session('success')}}</p>
          <p class="mb-0"></p>
        </div>
    @endif
    <form action="{{route ('send-mail')}}" method="POST" enctype="multipart/form-data">
        @csrf

        <label for="">Mail</label>
        <input type="email" name="adress" required>
        <label for="">Subject</label>
        <input type="text" name="subject" required>
        <label for="">message</label>
        <textarea name="msg"  rows="3" ></textarea>
        <label for="">attached</label>
        <input type="file" name="attached">
    <button type="submit" name="" id="" class="btn btn-primary" btn-lg btn-block">send mail</button>


</body>
</html>
