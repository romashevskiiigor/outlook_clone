<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;


Route::get('/', function () {
    return view('home');
})->name('home');

Route::post('send-mail', function(){
    Mail::to(request()->adress)->send(new SendMail(request()->msg, request()->subject, request()->attached));
    return redirect()->route('home')->with('success', 'message sent');
})-> name('send-mail');
